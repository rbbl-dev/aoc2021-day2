use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

fn main() {
    if let Ok(lines) = read_lines("./input.txt") {
        let mut distance = 0;
        let mut depth = 0;
        let mut aim = 0;
        for line in lines {
            if let Ok(line_string) = line {
                let split: Vec<&str> = line_string.split(" ").collect();
                let instruction = split[0];
                let value = split[1].parse::<i32>().unwrap();
                match instruction {
                    "forward" => {
                        distance += value;
                        depth += aim * value;
                    }
                    "up" => aim -= value,
                    "down" => aim += value,
                    _ => println!("unknown")
                }
            }
        }
        println!("depth: {} distance: {} multiplied: {}", depth, distance, depth * distance)
    }
}


// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

